class Server < ApplicationRecord
	validates_presence_of :nome, :url
	has_many :login
	has_many :users, through: :login
end
