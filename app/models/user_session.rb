	class UserSession
	include ActiveModel::Model
	attr_accessor :nome, :password, :token
	validates_presence_of :nome, :password, :token
	

	def initialize(session, attributes={})
		@session = session
		@nome = attributes[:nome]
		@password = attributes[:password]
		@token = attributes[:token]
	end

	def authenticateToken
		require 'uri'
		require 'net/http'
		
		url = URI("https://pessoaweb.herokuapp.com/token")
		
		http = Net::HTTP.new(url.host, url.port)
		http.use_ssl = true
		http.verify_mode = OpenSSL::SSL::VERIFY_NONE
		
		request = Net::HTTP::Get.new(url)
		request["cache-control"] = 'no-cache'
		request["postman-token"] = '0b64fd02-eb12-8da5-43c2-af4ffa0975a9'
		
		response = http.request(request) 
		
		validates_token = response.read_body
		puts "Token"
		puts validates_token

		if @token == validates_token
			true
		else
			errors.add(:base, :invalid_login)
			false
		end
	end	

	def authenticate!
		user = User.authenticate(@nome, @password)
		if user.present?
			store(user)
		else
		errors.add(:base, :invalid_login)
			false
		end
	end

	def store(user)
		@session[:user_id] = user.id
	end

	def current_user
		User.find(@session[:user_id])
	end

	def user_signed_in?
		@session[:user_id].present?
	end

	def destroy
		@session[:user_id] = nil
	end

end
