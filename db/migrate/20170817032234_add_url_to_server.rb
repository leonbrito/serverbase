class AddUrlToServer < ActiveRecord::Migration[5.1]
  def change
    add_column :servers, :url, :string
  end
end
