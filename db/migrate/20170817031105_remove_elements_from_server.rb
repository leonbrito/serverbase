class RemoveElementsFromServer < ActiveRecord::Migration[5.1]
  def change
    remove_column :servers, :login
    remove_column :servers, :password_digest
  end
end
